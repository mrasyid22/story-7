from django.shortcuts import render, redirect
from django.views import generic

def homepage(request):
    if request.method == 'GET':
        return render(request, 'about.html')
        
def redirecting(request):
    return redirect('/about/')
