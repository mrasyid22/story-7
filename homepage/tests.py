from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from homepage.views import homepage, redirecting

class UnitTest(TestCase):
    #Test views & urls
    def test_home_url_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist_is_not_exist(self):
        response = Client().get('/nothing/')
        self.assertEqual(response.status_code, 404)

    def test_home_using_homepage_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')
    
    def test_home_using_homepage_function(self):
        response = resolve('/about/')
        self.assertEqual(response.func, homepage)
