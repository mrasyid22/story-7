$(document).ready(function(){
    $(".accordion_header").click(function(){
        if($(this).hasClass("active")){
            $(this).removeClass("active")
        }
        else{
            $(".accordion_header").removeClass("active")
            $(this).addClass("active")
        }
        
    });

    $(".theme").change(function(){
        if ($("body").hasClass('dark')){
            $('body').removeClass("dark")
            $('body').addClass("light")
        } 
        else {
            $('body').removeClass("light")
            $('body').addClass("dark")
        }
    })
});
