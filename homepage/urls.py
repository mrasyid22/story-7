from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.redirecting, name='redirect'),
    path('about/', views.homepage, name='home'),
]
